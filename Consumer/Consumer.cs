﻿using DBAccess;
using Microsoft.EntityFrameworkCore;
using Serilog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Consumer
{
    internal class Consumer : IDisposable
    {
        public Config Config { get; }
        public int ConsumerId { get; }
        private readonly TasksContext _dbContext;
        private readonly TaskRepository _taskRepository;

        private static readonly Random Random = new Random();

        public Consumer(Config config, int consumerId)
        {
            Config = config;
            ConsumerId = consumerId;
            _dbContext = new TasksContext(Config.ConnectionString);
            _taskRepository = new TaskRepository(_dbContext);
        }

        public async Task Run(CancellationToken token)
        {
            Log.Information("Consumer {consumerId} started...", ConsumerId);
            var isWaiting = false;

            while (!token.IsCancellationRequested)
            {
                var tasks = await GetPendingTasksAsync(Config.BulkSize);
                if (tasks.Count == 0)
                {
                    if (!isWaiting) Log.Information("Consumer {consumerId} is waiting for new tasks...", ConsumerId);
                    isWaiting = true;
                    await Task.Delay(Config.RetryDelay);
                    continue;
                }

                isWaiting = false;

                // modify ConsumerId, status
                if (!await SetTasksInProgressAsync(tasks))
                {
                    // update failed, another Consumer has already updated it, skip and get next set from DB
                    continue;
                }

                foreach (var task in tasks)
                {
                    await ProcessTask(task);
                }
            }
        }

        private async Task ProcessTask(TaskInfo task)
        {
            // print TaskText to console
            Console.WriteLine($"Consumer '{ConsumerId}' is processing task '{task.TaskText}'");
            // artificial delay to simulate task processing
            await Task.Delay(Random.Next(Config.MaxProcessingTime));
            // modify status, random value success/failed
            task.Status = Random.Next(100) < Config.ErrorRate ? DBAccess.TaskStatus.Error : DBAccess.TaskStatus.Done;
            await UpdateTaskAsync(task);
        }

        private async Task<bool> SetTasksInProgressAsync(List<TaskInfo> tasks)
        {
            foreach (var task in tasks)
            {
                task.ConsumerId = ConsumerId;
                task.Status = DBAccess.TaskStatus.InProgress;
                _taskRepository.Update(task);
            }
            try
            {
                await _dbContext.SaveChangesAsync();
                return true;
            }
            // Optimistic Concurrency handling
            catch (DbUpdateConcurrencyException ex)
            {
                Log.Debug(ex, "No tasks updated");
                // discard our changes because different values have been already saved to DB
                _dbContext.ChangeTracker.Clear();
                return false;
            }
        }

        private async Task UpdateTaskAsync(TaskInfo task)
        {
            _taskRepository.Update(task);
            await _dbContext.SaveChangesAsync();
        }

        private async Task<List<TaskInfo>> GetPendingTasksAsync(int bulkSize)
        {
            return await _taskRepository.GetNewTasksAsync(bulkSize);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
