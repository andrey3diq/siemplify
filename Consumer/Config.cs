﻿using Microsoft.Extensions.Configuration;
using Serilog;

namespace Consumer
{
    internal class Config
    {
        public string ConnectionString;
        public int ErrorRate = 10;
        public int MaxProcessingTime = 100;
        public int BulkSize = 10;
        public int Threads = 10;
        public int RetryDelay = 1000;

        internal void Init(IConfigurationRoot configuration)
        {
            ConnectionString = configuration.GetConnectionString("DefaultConnection");
            Log.Information("Connection string: {connectionString}", ConnectionString);

            int.TryParse(configuration.GetSection("Consumer")["Threads"], out Threads);
            int.TryParse(configuration.GetSection("Consumer")["BulkSize"], out BulkSize);
            int.TryParse(configuration.GetSection("Consumer")["MaxProcessingTime"], out MaxProcessingTime);
            int.TryParse(configuration.GetSection("Consumer")["ErrorRate"], out ErrorRate);
            int.TryParse(configuration.GetSection("Consumer")["RetryDelay"], out RetryDelay);
            Log.Information("Config: {@Config}", this);
        }
    }
}
