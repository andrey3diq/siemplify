﻿using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Consumer
{
    class Program
    {
        private static readonly Config config = new Config();

        static async Task Main(string[] args)
        {
            ConfigureApp();
            var cts = new CancellationTokenSource();
            Console.CancelKeyPress += (s, e) =>
            {
                Log.Warning("Stopping...");
                cts.Cancel();
                e.Cancel = true;
            };

            try
            {
                Log.Information("Starting Consumers");

                // Use index as a ConsumerId, start from 1
                var consumers = Enumerable.Range(1, config.Threads)
                    .Select(i => Task.Run(async () => { 
                        using var consumer = new Consumer(config, i); 
                        await consumer.Run(cts.Token); 
                    })).ToArray();

                await Task.WhenAll(consumers);

                Log.Information("Consumers stopped");
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Error running Consumers");
                throw ex;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static void ConfigureApp()
        {
            Log.Logger = new LoggerConfiguration()
                .Destructure.ByTransforming<Config>(c => new { c.ConnectionString, c.Threads, c.BulkSize, c.ErrorRate, c.MaxProcessingTime, c.RetryDelay })
                .WriteTo.Console()
                .CreateLogger();

            string basePath = Directory.GetParent(AppContext.BaseDirectory).FullName;
            Log.Information("Base path: {basePath}", basePath);

            var configuration = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json", false)
                .Build();
            Log.Information("Configuration loaded");

            config.Init(configuration);
        }
    }
}
