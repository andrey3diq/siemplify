# Producer - Consumer

![picture](screenshot.png)

## Setup

Edit initDB.sql if you want to change the default DB connection settings.

Default values: 

- Host: localhost
- DB: siemplifytasks
- User: siemplifytasks
- Password: securepassword

### Create DB

To create a DB run this command (replace postgres with admin user):

`psql -U postgres -f initDB.sql`

### Change config files

Please change the *DefaultConnection* values in the *appsettings.json* file in all project folders: *Producer*, *Consumer*, *Monitoring*.

### Producer config

- *Threads* - number of parallel Producers
- *BulkSize* - number of Tasks that one Producer creates at once
- *Delay* - time in milliseconds that Producer waits before next iteration

### Consumer config

- *Threads* - number of parallel Consumers
- *BulkSize* - number of Tasks that one Consumer takes for processing
- *MaxProcessingTime* - Task processing time is random, limited by MaxProcessingTime
- *ErrorRate* - Task processing result can be Success of Failed, this is the percent value of failures
- *RetryDelay* - time in milliseconds that Consumer waits before next DB query when there are no pending tasks in DB

## Build

### Windows

Run script `build-debug.cmd` or `build-release.cmd`.

### Other OS

Run command `dotnet build --configuration Release` for Release build.

Run command `dotnet build --configuration Debug` for Debug build.

## Start project

## Windows

Run script `start-debug.cmd` or `start-release.cmd`.

### Other OS

Start 3 terminal windows.

For Debug run `dotnet run --project Producer --configuration Debug`, `dotnet run --project Consumer --configuration Debug`, `dotnet run --project Monitoring --configuration Debug`.

For Release run `dotnet run --project Producer --configuration Release`, `dotnet run --project Consumer --configuration Release`, `dotnet run --project Monitoring --configuration Release`.
