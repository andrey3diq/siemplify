﻿using Bogus;

namespace Producer
{
    public static class FakeData
    {
        private static readonly Faker f = new Faker();
        public static string GetText()
        {
            return f.Hacker.Phrase();
        }
    }
}
