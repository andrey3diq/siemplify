﻿using DBAccess;
using Serilog;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Producer
{
    internal class Producer : IDisposable
    {
        public Config Config { get; }
        public delegate void ProducerEventHandler(int number);
        public event ProducerEventHandler OnProduced;
        private readonly TasksContext _dbContext;
        private readonly TaskRepository _taskRepository;

        public Producer(Config config, int n)
        {
            Config = config;
            _dbContext = new TasksContext(Config.ConnectionString);
            _taskRepository = new TaskRepository(_dbContext);
            Log.Information("Producer {n} started...", n);
        }

        public async Task Run(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                await CreateTasksAsync(Config.BulkSize);
                OnProduced?.Invoke(Config.BulkSize);
                if (Config.Delay > 0)
                {
                    await Task.Delay(Config.Delay);
                }
            }
        }

        private async Task CreateTasksAsync(int count)
        {
            // create specific number of TaskInfo entries
            var createTasks = Enumerable.Range(0, count)
                .Select(_ => _taskRepository.CreateAsync(FakeData.GetText()))
                .ToList();
            await Task.WhenAll(createTasks);
            await _dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
