﻿using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Producer
{

    class Program
    {
        private static readonly Config config = new Config();
        private static int taskCounter = 0;

        static async Task Main(string[] args)
        {
            ConfigureApp();
            var cts = new CancellationTokenSource();
            Console.CancelKeyPress += (s, e) =>
            {
                Log.Warning("Stopping...");
                cts.Cancel();
                e.Cancel = true;
            };

            try
            {
                Log.Information("Starting Producers");

                var producers = Enumerable.Range(1, config.Threads)
                    .Select(n => Task.Run(async () => {
                        using var producer = new Producer(config, n);
                        producer.OnProduced += (count) =>
                        {
                            taskCounter += count;
                            Console.Write($"\rProduced {taskCounter} tasks...");
                        };
                        await producer.Run(cts.Token);
                    })).ToArray();

                await Task.WhenAll(producers);

                Log.Information("Producers stopped");
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Error running Producers");
                throw ex;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }


        private static void ConfigureApp()
        {
            Log.Logger = new LoggerConfiguration()
                .Destructure.ByTransforming<Config>(c => new { c.ConnectionString, c.Threads, c.BulkSize, c.Delay })
                .WriteTo.Console()
                .CreateLogger();

            string basePath = Directory.GetParent(AppContext.BaseDirectory).FullName;
            Log.Information("Base path: {basePath}", basePath);
            
            var configuration = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json", false)
                .Build();
            Log.Information("Configuration loaded");

            config.Init(configuration);
        }
    }
}
