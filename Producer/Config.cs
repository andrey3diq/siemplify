﻿using Microsoft.Extensions.Configuration;
using Serilog;

namespace Producer
{
    internal class Config
    {
        public string ConnectionString;
        public int BulkSize = 10;
        public int Threads = 10;
        public int Delay = 1000;

        internal void Init(IConfigurationRoot configuration)
        {
            ConnectionString = configuration.GetConnectionString("DefaultConnection");
            Log.Information("Connection string: {connectionString}", ConnectionString);

            int.TryParse(configuration.GetSection("Producer")["Threads"], out Threads);
            int.TryParse(configuration.GetSection("Producer")["BulkSize"], out BulkSize);
            int.TryParse(configuration.GetSection("Producer")["Delay"], out Delay);
            Log.Information("Config: {@Config}", this);
        }
    }
}
