﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccess
{
    public class TaskRepository
    {
        public TasksContext TasksContext { get; set; }

        public TaskRepository(TasksContext tasksContext)
        {
            TasksContext = tasksContext;
        }

        public async Task CreateSchemaAsync()
        {
            await TasksContext.Database.EnsureCreatedAsync();
        }

        public async Task CreateAsync(string taskText)
        {
            var time = DateTime.Now;
            await TasksContext.TaskInfos.AddAsync(new TaskInfo
            {
                CreationTime = time,
                ModificationTime = time,
                Status = TaskStatus.Pending,
                TaskText = taskText
            });
        }

        public void Update(TaskInfo taskInfo)
        {
            taskInfo.ModificationTime = DateTime.Now;
            TasksContext.TaskInfos.Update(taskInfo);
        }

        public async Task<List<TaskInfo>> GetNewTasksAsync(int limit)
        {
            return await TasksContext.TaskInfos
                .OrderBy(t => t.Id)
                .Where(t => t.ConsumerId == null && t.Status == TaskStatus.Pending)
                .Take(limit)
                .ToListAsync();
        }

        public async Task<List<TaskInfo>> GetLatestTasksAsync(List<int> consumerIDs)
        {
            // the fastest way we can do with LINQ to SQL:
            // get MAX ModificationTime for each ConsumerId,
            // and join with Tasks on ModificationTime and ConsumerId
            var consumerLastModifiedTimes = TasksContext.TaskInfos
                .Where(t => t.ConsumerId.HasValue && consumerIDs.Contains(t.ConsumerId.Value))
                .GroupBy(k => k.ConsumerId, e => e.ModificationTime, (consumerId, e) => new { consumerId, lastTime = e.Max(x => x) });

            return await consumerLastModifiedTimes.Join(TasksContext.TaskInfos,
                o => new { Time = o.lastTime, ConsumerId = o.consumerId }, 
                i => new { Time = i.ModificationTime, ConsumerId = i.ConsumerId }, 
                (o, i) => i)
                .ToListAsync();
        }

        public async Task<Statistics> GetStatisticsAsync()
        {
            var tasksByStatus = await TasksContext.TaskInfos
                .GroupBy(t => t.Status)
                .Select(g => new { g.Key, LongCount = g.LongCount()})
                .ToDictionaryAsync(g => g.Key, g => g.LongCount);

            // LINQ to SQL Average function only works with int, calculate avg on client side
            var timeDiffMillis = await TasksContext.TaskInfos
                .Where(t => t.Status == TaskStatus.Done)
                .Select(t => (t.ModificationTime - t.CreationTime).TotalMilliseconds).ToListAsync();
            var avgTimeMillis = timeDiffMillis.Count > 0 ? timeDiffMillis.Average() : 0;
            var avgTime = TimeSpan.FromMilliseconds(avgTimeMillis);
            return new Statistics { 
                TasksByStatus = tasksByStatus, 
                AvgProcessingTime = avgTime 
            };
        }
    }
}
