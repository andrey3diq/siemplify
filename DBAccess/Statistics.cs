﻿using System;
using System.Collections.Generic;

namespace DBAccess
{
    public class Statistics
    {
        public Dictionary<TaskStatus, long> TasksByStatus { get; set; }
        public TimeSpan AvgProcessingTime { get; set; }
        public double ErrorRatio
        {
            get
            {
                TasksByStatus.TryGetValue(TaskStatus.Done, out var done);
                TasksByStatus.TryGetValue(TaskStatus.Error, out var errors);
                var total = done + errors;
                var errorRatio = total == 0 ? 0 : errors * 100.0d / total;
                return errorRatio;
            }
        }
    }
}
