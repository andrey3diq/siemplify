﻿using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace DBAccess
{
    public class TasksContext : DbContext
    {
        private readonly string _connectionString;
        public TasksContext(string connectionString) : base()
        {
            _connectionString = connectionString;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (optionsBuilder.IsConfigured) return;

            optionsBuilder.UseNpgsql(_connectionString);
            //optionsBuilder.LogTo(message => Debug.WriteLine(message));
        }

        public virtual DbSet<TaskInfo> TaskInfos { get; set; }
    }
}
