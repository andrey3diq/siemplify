﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DBAccess
{
    public class TaskInfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime ModificationTime { get; set; }
        public string TaskText { get; set; }
        public TaskStatus Status { get; set; }
        [ConcurrencyCheck]
        public int? ConsumerId { get; set; }
    }
}
