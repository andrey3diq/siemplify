﻿namespace DBAccess
{
    public enum TaskStatus
    {
        Pending,
        InProgress,
        Error,
        Done
    }
}