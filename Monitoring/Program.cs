﻿using DBAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Monitoring
{

    class Program
    {
        private static IConfigurationRoot configuration;
        private static string connectionString;
        private const int refreshDelay = 1000;

        static async Task Main(string[] args)
        {
            ConfigureApp();
            var cts = new CancellationTokenSource();
            Console.CancelKeyPress += (s, e) =>
            {
                Log.Warning("Stopping...");
                cts.Cancel();
                e.Cancel = true;
            };

            try
            {
                Log.Information("Starting Monitor with refresh interval {Refresh}", refreshDelay);

                var monitoringThread = Task.Run(() => Monitoring(cts.Token));
                await monitoringThread;

                Log.Information("Stopping Monitor");
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Error running Monitor");
                throw ex;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static async Task Monitoring(CancellationToken token)
        {
            using var db = new TasksContext(connectionString);
            var taskRepository = new TaskRepository(db);

            while (!token.IsCancellationRequested)
            {
                // get pending tasks
                var stats = await taskRepository.GetStatisticsAsync();
                Console.CursorTop = 7;
                Log.Information("TasksByStatus: {@TasksByStatus}", stats.TasksByStatus);
                Log.Information("AvgProcessingTime: {AvgProcessingTime}", stats.AvgProcessingTime);
                Log.Information("ErrorRatio: {ErrorRatio}", stats.ErrorRatio);

                var tasks = await taskRepository.GetLatestTasksAsync(new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 });
                Log.Information("Latest consumer tasks");
                foreach(var task in tasks.OrderBy(t => t.ConsumerId))
                {
                    Log.Information("{@Task}", task);
                }

                await Task.Delay(refreshDelay);
            }
        }

        private static void ConfigureApp()
        {
            Log.Logger = new LoggerConfiguration()
                .Destructure.ByTransforming<TaskInfo>(t => new { t.ConsumerId, t.Id, t.ModificationTime })
                .WriteTo.Console()
                .CreateLogger();

            string basePath = Directory.GetParent(AppContext.BaseDirectory).FullName;
            Log.Information("Base path: {BasePath}", basePath);

            configuration = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json", false)
                .Build();
            Log.Information("Configuration loaded");

            connectionString = configuration.GetConnectionString("DefaultConnection");
            Log.Information("Connection string: {ConnectionString}", connectionString);
        }
    }
}
